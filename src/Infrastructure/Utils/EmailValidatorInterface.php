<?php

namespace CursoCleanArch\Infrastructure\Utils;

interface EmailValidatorInterface
{
    public function isValidEmail(string $email): bool;
}