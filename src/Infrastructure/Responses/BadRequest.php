<?php

namespace CursoCleanArch\Infrastructure\Responses;

use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Response;

class BadRequest
{
    public function getResponse(array $error): Response
    {
        return new Response(
            status: 400,
            headers: new Headers(['Content-Type' => 'application/json']),
            body: (new StreamFactory())->createStream(json_encode(
                $error
            ))
        );
    }
}
