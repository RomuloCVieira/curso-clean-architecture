<?php

namespace CursoCleanArch\Infrastructure\Responses;

use Slim\Psr7\Headers;
use Slim\Psr7\Response;

class OKRequest
{
    public function getResponse(): Response
    {
        return new Response(
            status: 200,
            headers: new Headers(['Content-Type' => 'application/json'])
        );
    }
}