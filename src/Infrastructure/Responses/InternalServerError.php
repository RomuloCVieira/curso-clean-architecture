<?php

declare(strict_types=1);

namespace CursoCleanArch\Infrastructure\Responses;

use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Response;

class InternalServerError
{
    public function getResponse(array $error): Response
    {
        return new Response(
            status: 500,
            headers: new Headers(['Content-Type' => 'application/json']),
            body: (new StreamFactory())->createStream(json_encode(
                $error
            ))
        );
    }
}