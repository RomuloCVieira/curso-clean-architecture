<?php

namespace CursoCleanArch\Infrastructure\Exceptions;

use Exception;

class InternalServerErrorException extends Exception
{
}