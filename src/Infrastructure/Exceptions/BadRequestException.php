<?php

namespace CursoCleanArch\Infrastructure\Exceptions;

use Exception;

class BadRequestException extends Exception
{
}