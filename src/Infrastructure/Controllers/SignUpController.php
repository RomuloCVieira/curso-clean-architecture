<?php

declare(strict_types=1);

namespace CursoCleanArch\Infrastructure\Controllers;

use CursoCleanArch\Infrastructure\Exceptions\BadRequestException;
use CursoCleanArch\Infrastructure\Exceptions\InternalServerErrorException;
use CursoCleanArch\Infrastructure\Responses\BadRequest;
use CursoCleanArch\Infrastructure\Responses\InternalServerError;
use CursoCleanArch\Infrastructure\Responses\OKRequest;
use CursoCleanArch\Infrastructure\Utils\EmailValidatorInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class SignUpController implements ControllerInterface
{
    private const PARAMS_REQUIRED = [
        'name',
        'email',
        'password',
        'password_confirmation'
    ];
    private readonly EmailValidatorInterface $emailValidator;

    public function __construct(EmailValidatorInterface $emailValidator)
    {
        $this->emailValidator = $emailValidator;
    }
    public function handle(Request $request): ResponseInterface
    {
        try {
            $body = json_decode($request->getBody()->getContents(), true);

            foreach (self::PARAMS_REQUIRED as $param) {
                $paramNotExists = !array_key_exists($param, $body);
                if ($paramNotExists) {
                    return throw new BadRequestException("Missing param: $param");
                }
            }

            $emailIsValid = $this->emailValidator->isValidEmail($body['email']);
            if (!$emailIsValid) {
                return throw new BadRequestException("Invalid param: $param");
            }

            if ($body['password'] !== $body['password_confirmation']) {
                return throw new BadRequestException("Invalid param: password_confirmation");
            }

            return (new OKRequest())->getResponse();
        } catch (BadRequestException $e) {
            return (new BadRequest())->getResponse(
                error: ['error' => ['message' => $e->getMessage()]]
            );
        } catch (InternalServerErrorException $e) {
            return (new InternalServerError())->getResponse(
                error: ['error' => ['message' => $e->getMessage()]]
            );
        }
    }
}
