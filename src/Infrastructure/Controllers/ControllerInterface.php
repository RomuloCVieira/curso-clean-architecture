<?php

namespace CursoCleanArch\Infrastructure\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

interface ControllerInterface
{
    public function handle(Request $request): ResponseInterface;
}