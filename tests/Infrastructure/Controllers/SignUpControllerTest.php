<?php

declare(strict_types=1);

namespace tests\Infrastructure\Controller;

use CursoCleanArch\Infrastructure\Controllers\ControllerInterface;
use CursoCleanArch\Infrastructure\Controllers\SignUpController;
use CursoCleanArch\Infrastructure\Exceptions\InternalServerErrorException;
use CursoCleanArch\Infrastructure\Utils\EmailValidator;
use Tests\TestCase;

class SignUpControllerTest extends TestCase
{
    public function dataProvider(): array
    {
        $emailValidatorStub = $this->createMock(EmailValidator::class);
        $signUp = new SignUpController($emailValidatorStub);

        return [
           'signup' => [$signUp, $emailValidatorStub]
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @return void
     */
    public function testShouldReturn400IfNoNameIsProvided(
        ControllerInterface $signUp
    ): void {
        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'email' => 'romulo@example.com',
                'password' => 'password',
                'password_confirmation' => 'password',
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Missing param: name']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @return void
     */
    public function testShouldReturn400IfNoEmailIsProvided(
        ControllerInterface $signUp
    ): void {
        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'password' => 'password',
                'password_confirmation' => 'password',
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Missing param: email']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @return void
     */
    public function testShouldReturn400IfNoPasswordIsProvided(
        ControllerInterface $signUp
    ): void {
        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'email' => 'romulo@example.com',
                'password_confirmation' => 'password'
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Missing param: password']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @return void
     */
    public function testShouldReturn400IfPasswordConfirmationFails(
        ControllerInterface $signUp
    ): void {
        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'email' => 'romulo@example.com',
                'password' => 'password',
                'password_confirmation' => 'invalid-password'
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Invalid param: password_confirmation']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @return void
     */
    public function testShouldReturn400IfNoPasswordConfirmationIsProvided(
        ControllerInterface $signUp
    ): void {
        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'email' => 'romulo@example.com',
                'password' => 'password'
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Missing param: password_confirmation']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param ControllerInterface $signUp
     * @param $emailValidator
     * @return void
     */
    public function testShouldReturn400IfAnInvalidEmailIsProvided(
        ControllerInterface $signUp,
        $emailValidator
    ): void {
        $emailValidator->method('isValidEmail')->willReturn(false);

        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'email' => 'romulo@example.com',
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Invalid param: password_confirmation']]),
            $response->getBody()->getContents()
        );
    }

    /**
     * @dataProvider dataProvider
     *
     * @param SignUpController $signUp
     * @param $emailValidator
     * @return void
     */
    public function testShouldReturn500ifEmailValidatorThrows(
        SignUpController $signUp,
        $emailValidator
    ): void {
        $emailValidator->method('isValidEmail')->will(
            $this->throwException(new InternalServerErrorException('Internal Server Error'))
        );

        $request = $this->createRequest(
            method: 'POST',
            path: '/test-action-response-code',
            body: [
                'name' => 'Rômulo',
                'email' => 'romulo@example.com',
                'password' => 'password',
                'password_confirmation' => 'password'
            ]
        );

        $response = $signUp->handle($request);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals(
            json_encode(['error' => ['message' => 'Internal Server Error']]),
            $response->getBody()->getContents()
        );
    }
}
